CREATE TABLE users
(
    id SERIAL PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    created_at TIMESTAMP
    WITHOUT TIME ZONE,
    updated_at TIMESTAMP WITHOUT TIME ZONE
)

    --@block
    select *
    from users
    SELECT id,
        username,
        email
    FROM users
    ORDER BY RANDOM()
LIMIT 3;
--@block
    select *
    from users

    --@block
    SELECT id, username, email
    FROM users
    ORDER BY RANDOM() LIMIT 3;

--@block
drop table users;
    drop table posts;
    select *
    from posts


    --@block
    CREATE TABLE posts
    (
        id SERIAL PRIMARY KEY,
        title VARCHAR(255) NOT NULL,
        image VARCHAR(255) NOT NULL,
        place VARCHAR(255),
        content TEXT NOT NULL,
        created_at TIMESTAMP
        WITHOUT TIME ZONE,
        updated_at TIMESTAMP WITHOUT TIME ZONE,
        user_id INTEGER,
        FOREIGN KEY
        (user_id) REFERENCES users
        (id)
    )

        --@block 
        CREATE TABLE comments
        (
            id SERIAL PRIMARY KEY,
            content text NOT NULL,
            user_id integer,
            FOREIGN KEY (user_id) REFERENCES users(id),
            post_id integer,
            FOREIGN KEY (post_id) REFERENCES posts(id),
            created_at TIMESTAMP
            WITHOUT TIME ZONE,
        updated_at TIMESTAMP WITHOUT TIME ZONE
    )

            --@block
            CREATE TABLE likes
            (
                user_id INTEGER,
                FOREIGN KEY (user_id) REFERENCES users(id),
                post_id INTEGER,
                FOREIGN KEY (post_id) REFERENCES posts(id),
                islike BOOLEAN
            )
            --@block 
            ALTER TABLE trip ADD COLUMN end_date VARCHAR
            (255);

            --@block
            CREATE TABLE trip
            (
                id SERIAL PRIMARY KEY,
                city VARCHAR(255),
                start_date VARCHAR(255),
                end_date VARCHAR(255),
                day_length INTEGER,
                user_id INTEGER,
                FOREIGN KEY (user_id) REFERENCES users(id)
            )
            --@block
            drop table trip;

            --@block
            CREATE TABLE itinerary
            (
                id SERIAL PRIMARY KEY,
                day INTEGER,
                trip_id INTEGER,
                FOREIGN KEY (trip_id) REFERENCES trip (id)
            );
            select *
            from itinerary;
            --@block
            drop table itinerary;

            --@block
            CREATE TABLE sceneries
            (
                id SERIAL PRIMARY KEY,
                scenery_name VARCHAR(255),
                scenery_type VARCHAR(255),
                scenery_image varchar(255),
                scenery_description text,
                country_name VARCHAR(255),
                city_name VARCHAR(255),
                region_name VARCHAR(255)
            )


            --@block
            CREATE TABLE events
            (
                itinerary_id INTEGER,
                FOREIGN KEY (itinerary_id) REFERENCES itinerary (id),
                sceneries_id INTEGER,
                FOREIGN KEY (sceneries_id) REFERENCES sceneries (id),
                start_time VARCHAR(255),
                end_time VARCHAR(255)
            )

            --@block
            INSERT INTO trip
                (city, is_template, user_id)
            VALUES
                ('Tokyo', 'true', 1)
            --@block 
            INSERT INTO itinerary
                (start_time, end_time, sceneries_id, day, trip_id)
            VALUES
                ('10:00:00', '12:00:00', 1, 1, 1),
                ('13:00:00', '15:00:00', 4, 1, 1),
                ('16:00:00', '18:00:00', 7, 1, 1),
                ('10:00:00', '12:00:00', 2, 2, 1),
                ('13:00:00', '15:00:00', 5, 2, 1),
                ('16:00:00', '18:00:00', 8, 2, 1),
                ('10:00:00', '12:00:00', 3, 3, 1),
                ('13:00:00', '15:00:00', 6, 3, 1)
            --@block 
            SELECT *
            FROM users
            --@block
            DROP TABLE itinerary;
            --@block
            DROP TABLE events;
            --@block
            DROP TABLE trip;
            --@block
            DROP TABLE sceneries;
            --@block
            SELECT *
            FROM trip

            --@block
            SELECT *
            FROM events
            --@block
            SELECT *
            FROM itinerary
            --@block
            SELECT *
            FROM sceneries
            --@block 
            SELECT users.id, trip.id AS trip_id,
                itinerary.id AS itinerary_id,
                itinerary.day,
                events.start_time,
                events.end_time,
                sceneries.id AS sceneries_id
            FROM users LEFT JOIN trip on users.id = trip.user_id
                LEFT JOIN itinerary ON trip.id = itinerary.trip_id
                LEFT JOIN events ON events.itinerary_id = itinerary.id
                LEFT JOIN sceneries ON events.sceneries_id = sceneries.id
            --@block 
            SELECT users.*, trip.*
            FROM users LEFT JOIN trip on users.id = trip.user_id
                LEFT JOIN itinerary ON trip.id = itinerary.trip_id
                LEFT JOIN events ON events.itinerary_id = itinerary.id
                LEFT JOIN sceneries ON events.sceneries_id = sceneries.id

            --@block
            CREATE TABLE itinerary
            (
                id SERIAL PRIMARY KEY,
                day INTEGER,
                trip_id INTEGER,
                FOREIGN KEY (trip_id) REFERENCES trip (id)
            );
            select *
            from itinerary;
            --@block
            CREATE TABLE sceneries
            (
                id SERIAL PRIMARY KEY,
                scenery_name VARCHAR(255),
                scenery_type VARCHAR(255),
                scenery_image varchar(255),
                scenery_description text,
                country_name VARCHAR(255),
                city_name VARCHAR(255),
                region_name VARCHAR(255)
            )

            --@block
            CREATE TABLE events
            (
                itinerary_id INTEGER,
                FOREIGN KEY (itinerary_id) REFERENCES itinerary (id),
                sceneries_id INTEGER,
                FOREIGN KEY (sceneries_id) REFERENCES sceneries (id),
                start_time VARCHAR(255),
                end_time VARCHAR(255)
            )
            --@block
            INSERT INTO trip
                (city, is_template, user_id)
            VALUES
                ('Tokyo', 'true', 1)
            --@block 
            INSERT INTO itinerary
                (start_time, end_time, sceneries_id, day, trip_id)
            VALUES
                ('10:00:00', '12:00:00', 1, 1, 1),
                ('13:00:00', '15:00:00', 4, 1, 1),
                ('16:00:00', '18:00:00', 7, 1, 1),
                ('10:00:00', '12:00:00', 2, 2, 1),
                ('13:00:00', '15:00:00', 5, 2, 1),
                ('16:00:00', '18:00:00', 8, 2, 1),
                ('10:00:00', '12:00:00', 3, 3, 1),
                ('13:00:00', '15:00:00', 6, 3, 1)
            --@block 
            SELECT *
            FROM users
            --@block
            DROP TABLE itinerary;
            --@block
            DROP TABLE events;
            --@block
            DROP TABLE trip;
            --@block
            DROP TABLE sceneries;
            --@block
            drop table users;
            --@block
            drop table posts;
            --@block
            SELECT *
            FROM trip
            --@block
            SELECT *
            FROM events
            --@block
            SELECT *
            FROM itinerary
            --@block
            SELECT *
            FROM sceneries

            --@block 
            SELECT users.id,
                trip.id AS trip_id,
                itinerary.id AS itinerary_id,
                itinerary.day,
                events.start_time,
                events.end_time,
                sceneries.id AS sceneries_id
            FROM users
                LEFT JOIN trip on users.id = trip.user_id
                LEFT JOIN itinerary ON trip.id = itinerary.trip_id
                LEFT JOIN events ON events.itinerary_id = itinerary.id
                LEFT JOIN sceneries ON events.sceneries_id = sceneries.id
itinerary.id as itinerary_id,
            --@block
            SELECT trip.id as trip_id,  
            itinerary.day as itinerary_day, 
             json_agg( 
                json_build_object(
                    'itinerary_id', itinerary_id,
                    'sceneries_id', (select array_agg(scenery_items) from (select * from sceneries where sceneries.id = sceneries_id) as scenery_items),
                    'start_time', events.start_time,
                    'end_time', events.end_time 
                    ) order by events.start_time)  as itineraries 
            FROM users
                LEFT JOIN trip on users.id = trip.user_id
                LEFT JOIN itinerary ON trip.id = itinerary.trip_id
                LEFT JOIN events ON events.itinerary_id = itinerary.id
                LEFT JOIN sceneries ON events.sceneries_id = sceneries.id
            where itinerary_id is not null

            GROUP By trip.id, itinerary.id, itinerary.day
            order by trip.id, itinerary.day





            --@block 
            SELECT users.*,
                trip.*
            FROM users
                LEFT JOIN trip on users.id = trip.user_id
                LEFT JOIN itinerary ON trip.id = itinerary.trip_id
                LEFT JOIN events ON events.itinerary_id = itinerary.id
                LEFT JOIN sceneries ON events.sceneries_id = sceneries.id

            --@block
            CREATE TABLE trip
            (
                id SERIAL PRIMARY KEY,
                city VARCHAR(255),
                start_date VARCHAR(255),
                end_date VARCHAR(255),
                day_length INTEGER,
                user_id INTEGER,
                FOREIGN KEY (user_id) REFERENCES users(id)
            );
            CREATE TABLE itinerary
            (
                id SERIAL PRIMARY KEY,
                day INTEGER,
                trip_id INTEGER,
                FOREIGN KEY (trip_id) REFERENCES trip (id)
            );
            --@block
            CREATE TABLE events
            (
                itinerary_id INTEGER,
                FOREIGN KEY (itinerary_id) REFERENCES itinerary (id),
                sceneries_id INTEGER,
                FOREIGN KEY (sceneries_id) REFERENCES sceneries (id),
                start_time VARCHAR(255),
                end_time VARCHAR(255)
            );



            --@block
            DROP TABLE events;
            --@block
            DROP TABLE itinerary;
            --@block
            DROP TABLE trip;
            --@block
            DROP TABLE users;
            --@block
            DROP TABLE likes;
            --@block
            DROP TABLE comments;
            --@block
            DROP TABLE posts;
            --@block
            DROP TABLE sceneries;

            --@block function
            CREATE TABLE feedback
            (
                id SERIAL PRIMARY KEY,
                name VARCHAR(255),
                email VARCHAR(255),
                feedback TEXT,
                created_at TIMESTAMP
                WITHOUT TIME ZONE,
    updated_at TIMESTAMP WITHOUT TIME ZONE
)

                --@block
                select *
                from feedback;
