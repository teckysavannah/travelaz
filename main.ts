import dotenv from "dotenv";
dotenv.config();

import express from "express";
import { Request, Response, NextFunction } from "express";
import expressSession from "express-session";
import multer from "multer";
import path from "path";
import moment from "moment";
import { Client } from "pg";
import fetch from "node-fetch";
import grant from "grant";
import { hashPassword, checkPassword } from "./hash";
import http from "http";
import { Server as SocketIO } from "socket.io";

// Set up express application
const app = express();
const server = new http.Server(app);
const io = new SocketIO(server);

// URLENCODED => JS
app.use(express.urlencoded({ extended: true }));

// JSON string => JS
app.use(express.json());

// Connect to database
export const client = new Client({
  database: process.env.DB_NAME,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
});
client.connect();

// Create express session middleware
const sessionMiddleware = expressSession({
  secret: "Travelaz",
  resave: true,
  saveUninitialized: true,
});

app.use(sessionMiddleware);


// Customizing multer
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve("./uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
  },
});
const upload = multer({ storage });


// Google login grant
const grantExpress = grant.express({
  defaults: {
    origin: "http://localhost:8080",
    transport: "session",
    state: true,
  },
  google: {
    key: process.env.GOOGLE_CLIENT_ID || "",
    secret: process.env.GOOGLE_CLIENT_SECRET || "",
    scope: ["profile", "email"],
    callback: "/login/google",
  },
});

app.use(grantExpress as express.RequestHandler);

// interface User {
//   id: number;
//   username: string;
//   email: string;
//   password: string;
// }


// Path Track
app.use((req, res, next) => {
  const dateTime = moment(new Date(), "MM-DD-YYYY HH:mm:ss", true).format("YYYY-MM-DD HH:mm:ss");
  console.log(`[${dateTime}] Request: ${req.path}`);
  next();
});

// Companion route
app.get("/companion", async (req, res) => {
  try {let companionResult = await client.query(
    `SELECT id, username, email FROM users WHERE id != ${req.session["user"].id}
  ORDER BY RANDOM() LIMIT 3`
  );
  res.json(companionResult.rows);}
  catch(err){
    res.json([])
  }
});

// loginGoogle router handler
async function loginGoogle(req: Request, res: Response) {
  const accessToken = req.session?.["grant"].response.access_token;
  const fetchRes = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
    method: "get",
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
  
  const googleUserInfo: any = await fetchRes.json();
  let user = (await client.query(`SELECT * FROM users WHERE email = $1`, [googleUserInfo.email])).rows[0];
  if (!user) {
    const insertUserInfo = await client.query(
      `INSERT INTO users(username, email, password, created_at, updated_at) VALUES ($1,$2,$3,$4,$5)`,
      [googleUserInfo.name, googleUserInfo.email, " ", "NOW()", "NOW()"]
    );
    user = insertUserInfo.rows[0];
  }
  const { id, username, email } = user;
  if (req.session) {
    req.session["user"] = {
      id,
      username,
      email,
    };
  }
  res.redirect("/");
}


// Register for new user
app.post("/register", async (req, res) => {
  const { register_username, register_email, register_password } = req.body;
  const hashedPassword = await hashPassword(register_password);
  const userResult = await client.query(`SELECT * FROM users where email = $1`, [register_email]);
  if (register_username && register_password) {
    if (userResult.rowCount == 1) {
      res.status(400).json({ message: "Email has already been registered" });
      return;
    }
    await client.query(`INSERT INTO users (username,email,password,created_at,updated_at) VALUES ($1,$2,$3,$4,$5)`, [
      register_username,
      register_email,
      hashedPassword,
      "NOW()",
      "NOW()",
    ]);
    res.status(200).json({ message: "Registered successfully" });
  } else {
    res.status(400).json({ message: "Please enter username / password" });
  }
});


// Check whether user is logged in
app.get("/login", async (req, res) => {
  if (req.session["user"]) {
    res.json({ message: true });
  } else {
    res.json({ message: false });
  }
});

// Login for existing users
app.post("/login", async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    res.status(400).json({ message: "Missing email/password" });
    return;
  }
  const userResult = await client.query(`SELECT * FROM users where email = $1`, [email]);
  const user = userResult.rows[0];
  if (!user) {
    res.status(400).json({ message: "Invalid username/password" });
    return;
  }
  const isValidPassword = await checkPassword(password, user.password);
  if (!isValidPassword) {
    res.status(400).json({ message: "Invalid username/password" });
    return;
  }
  const { id, username } = user
  req.session["user"] = { id, username, email };
  res.status(200).end()
});

 // Google Login Middleware
 app.get("/login/Google", loginGoogle);

// Logout
app.get("/logout", (req, res) => {
  req.session.destroy(() => {
    res.redirect("/");
  });
});


// Likes routes
// app.post("/likes/:pid/:isLiked", async (req, res) => {
//   const uid = req.session["user"].id;
//   const pid = parseInt(req.params.pid);
//   const isLiked = req.params.isLiked;
//   if (isLiked === "true") {
//     await client.query(`INSERT INTO likes (user_id, post_id) VALUES ($1, $2)`, [uid, pid]);
//   } else {
//     await client.query(`DELETE FROM likes WHERE user_id = $1 AND post_id = $2`, [uid, pid]);
//   }
//   res.status(200).json({ message: "likes updated successfully" });
// });

// app.get("/likes/:pid", async (req, res) => {
//   const pid = parseInt(req.params.pid);
//   const queryResult = await client.query(`SELECT * FROM likes WHERE post_id = $1`, [pid]);
//   const numOfLikes = queryResult.rows.length;
//   res.status(200).json({ numOfLikes });
// });


// Get username
app.get("/users/:uid", async (req, res) => {
  const uid = parseInt(req.params.uid);
  const queryResult = await client.query(`Select username from users where id = $1`, [uid]);
  const username = queryResult.rows[0];
  res.json(username);
});

// Blog post routes
// Get posts
app.get("/posts", async (req: Request, res: Response) => {
  // const currentUserID = req.session["user"].id
  const queryResult = await client.query(`SELECT * FROM posts ORDER BY id DESC`);
  const posts = queryResult.rows; // [post1,post2]
  const user_id = req.session["user"].id;
  res.json({ currentUserID: user_id, posts }); // { posts: [post1,post2] } => '{ "posts": [post1,post2] }'
});

// Post posts
app.post("/posts", upload.single("image"), async (req, res) => {
  console.log("reach backend");
  const { title, place, content } = req.body;
  const image = req.file?.filename;
  if (!content || !title || !image) {
    res.status(400).json({ message: "Invalid Input" });
    return;
  }

  await client.query(
    `INSERT INTO posts (title, content, place, image, user_id, created_at, updated_at) VALUES ($1,$2, $3, $4, $5,NOW(),NOW())`,
    [title, content, place, image, req.session["user"].id]
  );
  io.emit("new post", { title, content, place, image });
  res.json({ message: "Success" });
});

// Delete posts
app.delete("/posts/:pid", async (req, res) => {
  const pid = parseInt(req.params.pid);
  await client.query(`DELETE FROM likes WHERE post_id = $1`, [pid]);
  await client.query(`DELETE FROM comments WHERE post_id = $1`, [pid]);
  await client.query(`DELETE FROM posts WHERE id = $1`, [pid]);
  io.emit("delete post", { message: "deleted" });
  res.json({ message: "Success" });
});

// Get post's comment
app.get("/comments", async (req, res) => {
  const queryResult = await client.query(/*sql*/ `
  SELECT posts.id AS posts_id, posts.content AS post_content, posts.created_at AS post_created_at, users.username,
    comments.*
  FROM posts
  LEFT JOIN comments
  ON posts.id = comments.post_id
  LEFT JOIN users
  ON comments.user_id = users.id
  ORDER BY comments.id DESC
  `);
  const rows = queryResult.rows;
  const sqlResultMap = new Map<
    number,
    {
      post_id: number;
      comment: Array<{
        user_id: number;
        username: string;
        content: string;
        created_at: string;
      }>;
    }
  >();


  for (const row of rows) {
    if (!sqlResultMap.has(row.posts_id)) {
      sqlResultMap.set(row.posts_id, {
        post_id: row.posts_id,
        comment: [],
      });
    }

    if (row.user_id) {
      sqlResultMap.get(row.post_id)?.comment.push({
        user_id: row.user_id,
        username: row.username,
        content: row.content,
        created_at: row.created_at,
      });
    }
  }
  res.json({ data: Array.from(sqlResultMap.values()) });
});


// Post comments
app.post("/comments", async (req, res) => {
  const content = req.body.content;
  const postID = parseInt(req.body.post_id);
  console.log(postID);
  if (!content) {
    res.status(400).json({ message: "Invalid Input" });
    return;
  }

  await client.query(
    `INSERT INTO comments (content, user_id, post_id, created_at, updated_at) VALUES ($1, $2, $3, $4, $5)`,
    [content, req.session["user"].id, postID, "NOW()", "NOW()"]
  );
  io.emit("new comment", { content, postID });
  console.log({ content, postID });
  res.json({ message: "success" });
});




// Get Scenery
app.post("/searchScenery", async (req, res) => {
  const city = req.body.city[0].toUpperCase() + req.body.city.slice(1);
  const type = req.body.type;
  console.log(city, type);
  const queryResult = await client.query(`SELECT * FROM sceneries WHERE city_name = $1 AND scenery_type = $2`, [
    city,
    type,
  ]);
  const result = queryResult.rows;
  res.json({ result });
});



// Delete trip
app.delete("/trip/:tid", async (req, res) => {
  const tid = parseInt(req.params.tid);
  const getItineraryIds = (await client.query(`SELECT id FROM itinerary WHERE trip_id = $1`, [tid])).rows;
  for (const itineraryID of getItineraryIds) {
    await client.query(`DELETE FROM events WHERE itinerary_id = $1`, [itineraryID.id]);
  }
  await client.query(`DELETE FROM itinerary WHERE trip_id = $1`, [tid]);
  await client.query(`DELETE FROM trip WHERE id = $1`, [tid]);
  res.status(200).json("Trip have been deleted");
});




// Get current trip info
app.get("/tripDay", async (req, res) => {
  const { trip_id, city, duration } = req.session["trip"];
  const user_id = req.session["user"].id;
  res.json({ user_id, trip_id, city, duration });
})


// Get all trip info
app.get("/tripInfo", async (req, res) => {
  try {
    const userID = req.session["user"].id;
    const trips = (await client.query(`SELECT * FROM trip WHERE user_id = $1`, [userID])).rows;
    let tripsInfo = [];
    for (let trip of trips) {
      tripsInfo.push(trip);
    }
    res.json(tripsInfo);
  } catch (err) {
    res.json([]);
  }
});



// Get scenery details of a trip
app.get("/itineraryDetails/:id", async (req, res) => {
  const dayID = parseInt(req.params.id);
  const city = req.session["trip"].city;
  const duration = req.session["trip"].duration;
  const searchTripID = req.session["trip"].trip_id;
  const userID = req.session["user"].id;
  const tripIdResult = await client.query(
    `SELECT id FROM trip WHERE city = $1 AND user_id = $2 AND day_length = $3 and id = $4`,
    [city, userID, duration, searchTripID]
  );
  let tripID = tripIdResult.rows[0].id;
  const itineraryResult = await client.query(
    `SELECT * FROM itinerary WHERE trip_id = $1 AND day = $2 ORDER BY id DESC`,
    [tripID, dayID]
  );
  let sceneryArr = [];
  for (const itinerary of itineraryResult.rows) {
    const day = itinerary.day;
    const sceneryList = await client.query(`SELECT * FROM events WHERE itinerary_id = $1`, [itinerary.id]);
    for (const scenery of sceneryList.rows) {
      const { start_time, end_time, sceneries_id } = scenery
      const sceneryInfo = (await client.query(`SELECT * FROM sceneries WHERE id = $1`, [sceneries_id])).rows[0];
      const { scenery_name, scenery_image, scenery_type, country_name, city_name, region_name } = sceneryInfo
      sceneryArr.push({
        trip_id: tripID,
        itinerary_id: itinerary.id,
        day,
        start_time,
        end_time,
        sceneries_id,
        scenery_name,
        scenery_image,
        scenery_type,
        country_name,
        city_name,
        region_name
      });
    }
  }
  res.json({ sceneryArr });
});


// Post trip itinerary 
app.post("/itinerary", async (req, res) => {
  const { original_trip_id, itineraries } = req.body;
  const getItineraryIds = (await client.query(`SELECT id FROM itinerary WHERE trip_id = $1`, [original_trip_id])).rows;
  for (const itineraryID of getItineraryIds) {
    await client.query(`DELETE FROM events WHERE itinerary_id = $1`, [itineraryID.id]);
  }
  await client.query(`DELETE FROM itinerary WHERE trip_id = $1`, [original_trip_id]);
  for (const itinerary of itineraries) {
    const nthDay = itinerary.nthDay;
    const sceneries = itinerary.sceneries;
    const itineraryIds = (
      await client.query(`INSERT INTO itinerary (trip_id, day) VALUES ($1, $2) RETURNING id`, [
        original_trip_id,
        nthDay,
      ])
    ).rows;
    for (const itinerary_id of itineraryIds) {
      const itineraryID = itinerary_id.id;
      for (const scenery of sceneries) {
        const sceneryID = scenery.event_id;
        const start_time = scenery.start_time;
        const end_time = scenery.end_time;
        await client.query(
          `INSERT INTO events(sceneries_id,itinerary_id,start_time, end_time) VALUES ($1, $2, $3,$4) `,
          [sceneryID, itineraryID, start_time, end_time]
        );
      }
    }
  }
  res.json({ message: "success" });
});


// Update req.session["trip"]
app.put("/tripSession", async (req, res) => {
  const { chosenTripID, chosenTripCity, chosenTripLength } = req.body;
  req.session["trip"] = { city: chosenTripCity, trip_id: chosenTripID, duration: chosenTripLength };
  res.status(200).json({ message: "Session updated successfully" });
});

// app.post("/renew_trip", async (req, res) => {
//   const original_trip_id = req.body.origin_trip_id;
//   const itineraries = req.body.itineraries;

//   // Get All Itinerary ID
//   const itinerary_ids_sql = `SELECT id FROM itinerary WHERE trip_id = ${original_trip_id}`;
//   const itinerary_ids = await (await client.query(itinerary_ids_sql)).rows;

//   const delete_events_sql = `DELETE FROM events WHERE itinerary_id IN [$1];`;
//   client.query(delete_events_sql, [itinerary_ids]);
//   // DELETE itinerary
//   const delete_itinerary_sql = `DELETE FROM itinerary WHERE trip_id = ${original_trip_id}`;

//   for (const itinerary of itineraries) {
//     // Insert itinerary
//     const scenery_ids = itinerary.scenery_ids;
//     const insert_itinerary_sql = `INSERT INTO itinerary (trip_id, day) VALUES ($1, $2)`;
//     const itinerary_id = await client.query(insert_itinerary_sql, [original_trip_id, itinerary.number_of_day]);

//     // Insert events
//     for (const scenery_id of scenery_ids) {
//       const insert_event_sql = `INSERT INTO event (sceneries_id, itinerary_id) VALUE [$1, $2]`;
//       const event_id = await client.query(insert_event_sql, [scenery_id, itinerary_id]);
//     }
//   }
// });
// app.delete("/tripDetails/:tid/:iid", async (req, res) => {
//   const tid = parseInt(req.params.tid);
//   const iid = parseInt(req.params.iid);
//   await client.query(`DELETE FROM events WHERE itinerary_id = $1`, [iid]);
//   await client.query(`DELETE FROM itinerary WHERE trip_id = $1`, [tid]);
//   // await client.query(`DELETE FROM trip WHERE id = $1`,[tid] )
//   res.json({ message: "success" });
// });

// app.post("/tripDetails/:tid", async (req, res) => {
//   const tid = parseInt(req.params.tid);
//   const { day, start_time, end_time, scenery_id } = req.body;
//   const itineraryIDList = await client.query(`INSERT INTO itinerary (trip_id, day) VALUES ($1, $2) RETURNING id`, [
//     tid,
//     day,
//   ]);
//   for (const itinerary of itineraryIDList.rows) {
//     const itineraryID = itinerary.id;
//     await client.query(
//       `INSERT INTO events (itinerary_id, start_time, end_time, sceneries_id) VALUES ($1, $2, $3, $4)`,
//       [itineraryID, start_time, end_time, scenery_id]
//     );
//   }
//   res.json({ message: "itinerary" });
// });


// Post random itinerary
app.post("/itineraryDetails", async (req, res) => {
  const { city, start_date, end_date } = req.body;
  let duration = Math.ceil((new Date(end_date).getTime() - new Date(start_date).getTime()) / 1000 / 60 / 60 / 24) + 1;
  const timeSlot = ["10:00", "13:00", "16:00", "19:00"];
  const userID = req.session["user"].id;
  const cityName = city;
  const queryResult = await client.query(
    `INSERT INTO trip (user_id,start_date,end_date,day_length,city) VALUES ($1, $2, $3, $4, $5) RETURNING id`,
    [userID, start_date, end_date, duration, cityName]
  );
  const tripID = queryResult.rows[0].id;
  req.session["trip"] = { trip_id: tripID, city, duration };
  const randomEvents = await client.query(`SELECT * FROM sceneries WHERE city_name = $1 ORDER BY RANDOM() LIMIT $2 `, [
    city,
    duration * 4,
  ]);

  const itineraryList: number[] = [];
  for (let i = 0; i < duration; i++) {
    const itineraryInsertResult = await client.query(
      `INSERT INTO itinerary (day, trip_id) VALUES ($1, $2) RETURNING id`,
      [i + 1, tripID]
    );
    itineraryList.push(itineraryInsertResult.rows[0].id);
  }

  for (let i = 0; i < randomEvents.rows.length; i++) {
    const event = randomEvents.rows[i];
    const sceneries_id = event.id;
    const dayIndex = Math.floor(i / 4);
    const timeSlotIdx = i % 4;
    const start_time_hour = timeSlot[timeSlotIdx];
    event["start_time"] = start_time_hour;
    let getHour = parseInt(start_time_hour) + 2;
    if (getHour < 10) {
      getHour = parseInt(getHour.toString().padStart(2, "0"));
    }
    const end_time_hour = `${getHour}:00`;
    event["end_time"] = end_time_hour;
    await client.query(
      `INSERT INTO events (start_time, end_time, itinerary_id, sceneries_id) VALUES ($1, $2, $3, $4)`,
      [event.start_time, event.end_time, itineraryList[dayIndex], sceneries_id]
    );
  }
  res.status(200).end();
});



// Feedback route
app.post("/contact", async (req, res) => {
  try {
    const name = req.body.name;
    const email = req.body.email;
    const feedbackText = req.body.feedbackText;
    await client.query(
      `INSERT INTO feedback (name, email, feedback, created_at, updated_at) VALUES ($1, $2, $3, $4, $5)`,
      [name, email, feedbackText, "NOW()", "NOW()"]
    );
    res.status(200).json({ message: "Success" });
  } catch (err) {
    res.status(400).json({ message: "Invalid Input" });
  }
});


export const isLoggedIn = (req: Request, res: Response, next: NextFunction) => {
  if (req.session["user"]) {
    next();
    return;
  }
  res.status(404).redirect("/unAuth.html");
};

app.use(express.static(path.resolve("public")));
app.use(isLoggedIn, express.static(path.resolve("protected")));
app.use("/images", express.static(path.resolve("uploads")));
app.use(express.static(path.resolve("dataset/images")));

app.use((req, res) => {
  res.sendFile(path.resolve("public/404.html"));
});

io.on("connection", async (socket) => {
  console.log(`[INFO] socketID: ${socket.id}`);
});

const PORT = 8080;
server.listen(PORT, () => {
  console.log(`Listening to http://localhost:${PORT}`);
});
