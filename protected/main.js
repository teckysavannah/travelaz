window.onload = async () => {
    await checkAuthState()
    await getTripDay()
    await getItineraryDetails()
    main()
}


// Drag and drop logic
function main() {
    let cards = document.querySelectorAll(".card");
    let lists = document.querySelectorAll(".list");

    cards.forEach((card) => {
        const dragEndHandler = (e) => {
            console.log("hello")
            const card = e.target;
            card.classList.remove("dragging");

            if (!card.parentNode.classList.contains('draggingContainer')) {
                card.remove();
            }
        }

        const dragStartHandler = (e) => {
            const card = e.target;
            if (card.classList.contains("scenery")) {
                const clone = card.cloneNode(true);
                clone.addEventListener("dragstart", dragStartHandler);
                clone.addEventListener("dragend", dragEndHandler);

                card.after(clone);
                card.classList.remove("scenery");
                card.classList.add("dragging");
                card.classList.add("itinerary");

            } else {
                card.classList.add("dragging");
                card.classList.add("itinerary");
            }
        }

        card.addEventListener("dragstart", dragStartHandler)
        card.addEventListener("dragend", dragEndHandler)
    })


    lists.forEach((list) => {
        list.addEventListener("dragover", (e) => {
            e.preventDefault();
            let draggingCard = document.querySelector(".dragging");
            if (!draggingCard) {
                return;
            }
            let cardAfterDraggingCard = getCardAfterDraggingCard(list, e.clientY)
            if (cardAfterDraggingCard) {
                cardAfterDraggingCard.parentNode.insertBefore(draggingCard, cardAfterDraggingCard)
                console.log("hi")
            } else {
                let draggingContainer = list.querySelector('.draggingContainer')
                draggingContainer.appendChild(draggingCard)
            }
        })
    })
}

function getCardAfterDraggingCard(list, yDraggingCard) {
    let listCards = [...list.querySelectorAll(".card:not(.dragging)")]
    return listCards.reduce((closestCard, nextCard) => {
        let nextCardRect = nextCard.getBoundingClientRect();
        let offset = yDraggingCard - nextCardRect.top - nextCardRect.height / 2;
        if (offset < 0 && offset > closestCard.offset) {
            return { offset, element: nextCard }
        } else {
            return closestCard;
        }
    }, { offset: Number.NEGATIVE_INFINITY }).element;
}

// Reset time slot
function resetTimeSLot() {
    const allTimeInput = document.querySelector(".itinerary-planner").querySelectorAll("input")
    for (let time of allTimeInput) {
        time.value = ''
    }
}


// Show sceneries from different catagories
const typeBtns = document.querySelectorAll(".type-btn");
for (let typeBtn of typeBtns) {
    typeBtn.addEventListener("click", async (e) => {
        e.preventDefault()
        const type = typeBtn.querySelector("span").innerHTML
        const resp = await fetch("/tripDay");
        const city = (await resp.json()).city
        let formObject = { city, type }
        const res = await fetch("/searchScenery", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formObject)
        })
        const draggingSceneryContainer = document.querySelector(".draggingContainer_scenery")
        const data = (await res.json()).result
        let html = ""
        for (let item of data) {
            console.log(item)
            html += /*html*/`<div class="card scenery draggable" draggable="true" data-id=${item.id} data-city='${item.city_name}'>
            <div class="container-box">
            <div class="title">
            <form>
                <input type='time' class="start_time" required>
                <input type='time' class="end_time" required>
            </form>
                <div>
              ${item.scenery_name}
                </div>
                <span>${item.region_name}, ${item.city_name}, ${item.country_name}</span>
            </div>
            <img src=${item.scenery_image} style="width: 100px;height: 100px" draggable="false">
            </div>
        </div>`
        }
        draggingSceneryContainer.innerHTML = html
        main();
    })

}

// Save modified itinerary
const saveBtn = document.querySelector(".save-btn");
saveBtn.addEventListener("click", async (e) => {
    e.preventDefault();

    const dayContainers = document.querySelectorAll(".itinerary-list");
    let data = {
        itineraries: []
    }
    for (let dayContainer of dayContainers) {
        data["original_trip_id"] = dayContainer.dataset.trip_id;
        const day = dayContainer.dataset.day
        const draggingContainer = dayContainer.querySelector(".draggingContainer");
        const itinerary = {
            nthDay: day,
            sceneries: []
        }
        const scenery_list = draggingContainer.querySelectorAll(".card");
        for (let i = 0; i < scenery_list.length; i++) {
            const start_time = scenery_list[i].querySelector("input.start_time").value
            const end_time = scenery_list[i].querySelector("input.end_time").value
            const event = {
                event_id: scenery_list[i].dataset.id,
                start_time,
                end_time
            }
            itinerary.sceneries.push(event)
        }
        data.itineraries.push(itinerary)
    }
    const resp = await fetch("/itinerary", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
    })
    if (resp.status === 200) {
        Swal.fire({
            icon: 'success',
            title: 'Trip saved',
            showConfirmButton: false,
            timer: 1500
        })
    }
    setTimeout(() => {
        window.location.replace("/")
    }, 2000)
})

// Get total trip day
async function getTripDay() {
    const res = await fetch("/tripDay");
    const result = await res.json();
    let html = ""
    for (let i = 0; i < result.duration; i++) {
        html += /*html*/`<div class="list itinerary-list" data-day=${i + 1} data-trip_id=${result.trip_id} data-city='${result.city}'>
        <h3>Day${i + 1}</h3>
        <div class="draggingContainer"> 
        </div>
        </div>`
    }
    document.querySelector(".trip").innerHTML = html;
}


// Check login or not
async function checkAuthState() {
    const res = await fetch('/login')
    const result = (await res.json()).message
    if (result) {
        document.querySelector('.logout-btn').classList.remove('display-none')
        document.querySelector('.login-btn').classList.add('display-none')

    } else {
        document.querySelector('.logout-btn').classList.add('display-none')
        document.querySelector('.login-btn').classList.remove('display-none')

    }
}

// Get itinerary details of a trip
async function getItineraryDetails() {
    const itineraryBlockNo = [...document.querySelectorAll(".itinerary-list")].length
    for (let i = 0; i < itineraryBlockNo; i++) {
        const res = await fetch(`/itineraryDetails/${i + 1}`);
        const sceneryList = (await res.json()).sceneryArr;
        let html = ''
        for (const scenery of sceneryList) {
            html += /*html*/
                `<div class="card itinerary draggable" draggable="true" data-id=${scenery.sceneries_id} data-city='${scenery.city_name}'>
                <div class="container-box">
                    <div class="title">
                        <form>
                            <input type='time' class="start_time" value=${scenery.start_time}>
                            <input type='time' class="end_time" value=${scenery.end_time}>
                        </form>
                        <div>
                            ${scenery.scenery_name}
                        </div>
                        <span>${scenery.region_name},</span><span>${scenery.city_name}</span><span>,${scenery.country_name}</span>
                    </div>
                    <img src=${scenery.scenery_image} style="width: 100px;height: 100px; border-radius:5px" draggable="false"> 
                </div>
             </div>
        `
        }
        document.querySelector(`[data-day='${(i + 1)}']`).querySelector(".draggingContainer").innerHTML = html
    }

}



