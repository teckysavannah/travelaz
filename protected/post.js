window.onload = async () => {
    await checkAuthState()
    const socket = io.connect()
    await loadBlogPage()
    
    socket.on('new post', async () => {
        await loadBlogPage()
    })

    socket.on('new comment', async () => {
        await displayComment()
    })

    socket.on('delete post', async () => {
        await loadBlogPage()
    })

}


generatePostModal()

async function loadBlogPage() {
    await displayPost()
    await addNewComment()
    const posts = document.querySelectorAll('.post')
    for (const post of posts) {
        await displayComment()
    }
}

async function checkAuthState() {
    const res = await fetch('/login')
    const result = (await res.json()).message

    const myTripContainer = document.querySelector(".my-trips-container")
    if (result) {
        document.querySelector('.logout-btn').classList.remove('display-none')
        document.querySelector('.login-btn').classList.add('display-none')

    } else {
        document.querySelector('.logout-btn').classList.add('display-none')
        document.querySelector('.login-btn').classList.remove('display-none')

    }
}


function generatePostModal() {
    document.querySelector('.newPostModal').innerHTML = /*html*/ `
  <!-- Modal -->
  <div class="modal fade" id="addPostModal" tabindex="-1" aria-labelledby="addPostModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title" id="addPostModalLabel">ADD NEW POST</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
        <form id="add-post">
        <div>
            <input class="m-1" type="file" name="image" id="image" required>
        </div>
        <div>
            <label for="title">Title:
                <input class="m-1" type="text" name="title" id="title" required>
            </label>
        </div>
        <div>
            <label for="place">Location:
                <input class="m-1" type="text" name="place" id="place">
            </label>
  
        </div>
        <div>
            <label for="content">Description:
                <textarea class="m-1" name="content" id="content" maxlength="800" style="width: 250px" required></textarea>
            </label>
        </div>
        <div>
            <input class="m-1" type="submit" value="ADD POST" data-bs-dismiss="modal" id="addPostBtn">
        </div>
    </form>
        </div>
      </div>
    </div>
  </div>
  `
}



async function deletePost(pid) {
    console.log("delete")
    const res = await fetch(`/posts/${pid}`, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json"
        }
    });
    if (res.status === 200) {
        await loadBlogPage()
    }
}

async function addNewComment() {
    const addComments = document.querySelectorAll('#add-comment')
    for (const addComment of addComments) {
        addComment.addEventListener('submit', async (e) => {
            e.preventDefault()
            const content = addComment['content'].value
            const post_id = addComment.dataset.id
            const formObject = { content, post_id }
            const res = await fetch('/comments', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(formObject),
            })
            addComment.reset()
            if (res.status === 200) {
                await displayComment()
            } else {
                // res: {"message": "XXX"}
                const data = await res.json() // JSON String => JS Object { message: "XXX" }
                const errMessage = data.message
                console.log(errMessage)
            }
        })
    }
}


// async function updateLikeCount(e, pid) {
//     e.target.classList.toggle('ha');
//     const likes = e.target.dataset.likes
//     // addLike
//     let isLike = e.target.classList.contains("ha")
//     const res = await fetch(`/likes/${pid}/${isLike}`, {
//         method: 'POST',
//         headers: {
//             "Content-Type": "application/json"
//         }
//     })
//     const likesRes = await fetch(`/likes/${pid}`)
//     const numOfLikes =  (await likesRes.json()).numOfLikes
//     e.target.dataset.likes = numOfLikes
//     e.target.parentNode.querySelector(".likes-count").innerHTML = numOfLikes
//     // const likes_no = (await res.json()).likeCount
//     // console.log(likes_no)
//     // console.log(e.target.parentNode)
//     // e.target.parentNode.querySelector(".likes-count").innerHTML = likes_no;
//     // e.target.dataset.likes = likes_no;
// }

function commentBtnClicked(pid) {
    document.querySelector(`#post${pid}`).classList.toggle('show')
}


async function displayPost() {
    const res = await fetch('/posts')
    const { currentUserID, posts } = await res.json()
    let html = ''
    let num = 0
    for (const post of posts) {
        const uid = post.user_id
        const pid = post.id
        const resp = await fetch(`/users/${uid}`)
        // const likesRes = await fetch(`/likes/${pid}`)
        // const numOfLikes =  (await likesRes.json()).numOfLikes
    //    let displayIcon = numOfLikes>0 ? "far fa-heart" : "fas fa-heart"
        const username = (await resp.json()).username
        // 如果post既user_id同currentUserID係一樣既話，即係個post係current user post既，咁就俾佢可以edit/delete自己既post
        const postPlace = post.place ? `<p>${post.place}</p>` : ``
        const currentUserSignal =
            uid == currentUserID ? `<i class="fas fa-times" onclick="deletePost(${post.id})"></i>` : ''
        const createdTime = new Date(post.created_at).toLocaleString()
        html += /*html*/ `
        <div class="post" id=${post.id}>
            <div class="header">
                <div class="personal">
                    <img src="/images/propic.jpeg" alt="ProPic">
                    <div>
                        <h3>${username}</h3>
                        <p>${post.place}</p>
                    </div>
                </div>
                <div class="editBtn">
                    ${currentUserSignal}
                </div>
            </div>
            <div class="title">${post.title}</div>
            <div class="post-image">
                <img style="width: 100%; height:400px" src="/images/${post.image}">
                <div>
                    Uploaded at <span>${createdTime}</span>
                </div>
            </div>
            <div>
                ${post.content}
                <button class="commentBtn" onclick="commentBtnClicked(${post.id})"><i class="far fa-comment-dots"></i></button>
            </div>
            <div class="comment-wall" id=post${post.id}></div>
            <form id="add-comment" data-id=${post.id}>
                <input type="text" name="content" id="content" placeholder="Add a comment" required>
                <button type="submit" class="btn btn-success" >
                <i class="fas fa-check-circle"></i></button>
            </form>
        </div>
    `
    }
    document.querySelector('.post-wall').innerHTML = html
}


const addPost = document.getElementById('add-post')
addPost.addEventListener('submit', async (e) => {
    console.log('submitting form')
    e.preventDefault()
    const formData = new FormData(addPost)
    const res = await fetch('/posts', {
        method: 'POST',
        body: formData,
    })
    addPost.reset()
    if (res.status === 200) {
        await displayPost()
        await addNewComment()
    } else {
        const errMessage = (await res.json()).message
        console.log(errMessage)
    }
})

async function displayComment() {
    const res = await fetch('/comments')
    const comments = (await res.json()).data
    for (const comment of comments) {
        let html = ''
        const targetCommentWall = document.querySelector(
            `#post${comment.post_id}`
        )
        for (const message of comment.comment) {
            html +=/*html*/ `
        <div class="comment">
            <div class="personal">
                <img src="/images/propic.jpeg" alt="ProPic" style="width: 25px; height: 25px">
                <span>${message.username}</span> 
            </div>
            <p>${message.content}</p>
        </div>
        `
        }
        targetCommentWall.innerHTML = html
    }
}
