export async function checkAuthState() {
    const res = await fetch('/login')
    const result = (await res.json()).message
    
    if (result) {
        document.querySelector('.logout-btn').classList.remove('display-none')
        document.querySelector('.login-btn').classList.add('display-none')
        
    } else {
        document.querySelector('.logout-btn').classList.add('display-none')
        document.querySelector('.login-btn').classList.remove('display-none')
       
    }
}