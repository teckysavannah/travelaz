export function header() {
  document.querySelector('header').innerHTML = /*html*/ `
    <div class="nav">
    <div class="brand">

      <a href="index.html"> <img class="logo" src="images/final_logo.png" alt="Logo"></a>
    </div>
    <nav>
      <ul>
        <li><a href="about.html">ABOUT</a></li>
        <li><a href="contact.html">CONTACT</a></li>
        <li><a href="post.html">BLOG</a></li>
        <li class="login-btn"><a href="login.html">LOGIN</a></li>
        <li class="logout-btn display-none"><a href="/logout">LOGOUT</a></li>
        <li>
          <input type="search" name="search" id="search" class="fas fa-search" placeholder="&#xf002"
            style="font-family:'Font Awesome 5 Free'">
        </li>
      </ul>
    </nav>
    <i class="fas fa-globe-americas"></i>
    <!-- <img class="menu-bar" src="menu.png" alt="Menu"> -->
  </div>
  `
  const menuBtn = document.querySelector(".fa-globe-americas");
  const navbar = document.querySelector("nav")
  console.log(navbar.classList)
  menuBtn.addEventListener("click", () => {
    navbar.classList.toggle("show")
  })


  const logoutBtn = document.querySelector(".logout-btn")
  logoutBtn.addEventListener("click", async () => {
    await fetch("/logout");
    console.log("hi")
  })
}
