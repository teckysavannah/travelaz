import { header } from './navbar.js'

window.onload = async () => {
    header()
    await checkAuthState()
    await companions()
    await showMyTripDetail()
    cancelTrip()
}

// Check login or not
async function checkAuthState() {
    const res = await fetch('/login')
    const result = (await res.json()).message
    console.log(result)
    const searchContainer = document.querySelector('.search-container')
    const potentialCompanion = document.querySelector(
        '.potential-companions-container'
    )
    const myTripContainer = document.querySelector(".my-trips-container")
    if (result) {
        document.querySelector('.logout-btn').classList.remove('display-none')
        document.querySelector('.login-btn').classList.add('display-none')
        searchContainer.classList.remove('display-none')
        potentialCompanion.classList.remove('display-none')
        myTripContainer.classList.remove('display-none')
    } else {
        document.querySelector('.logout-btn').classList.add('display-none')
        document.querySelector('.login-btn').classList.remove('display-none')
        searchContainer.classList.add('display-none')
        potentialCompanion.classList.add('display-none')
        myTripContainer.classList.add('display-none')
    }
}

const search_form = document.querySelector('#search-form')


// Cancel Trip Plan Btn 
function cancelTrip() {
    const cancelTripPlan = document.querySelectorAll('.cardcontent .fa-times')
    for (const cancelBtn of cancelTripPlan) {
        cancelBtn.addEventListener('click', async (e) => {
            const tid = e.target.dataset.trip
            Swal.fire({
                title: 'Do you want to save the changes?',
                showDenyButton: true,
                confirmButtonText: 'Delete',
                denyButtonText: `Quit`,
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    Swal.fire('Deleted!', '', 'success')
                    deleteTrip(tid)
                } else if (result.isDenied) {
                    Swal.fire('Nothing is deleted', '', 'info')
                }
            })
        })
    }
}


// Random Gen Potential Companion
async function companions() {
    const img = [
        'images/companion1.jpeg',
        'images/companion2.jpeg',
        'images/companion3.jpeg',
    ]
    const resp = await fetch('/companion')
    const tripCompanions = await resp.json()

    document.querySelector(
        '.potential-companions-container'
    ).innerHTML = /*html*/ `
<h1 class="widget-title">Potential Companions</h1>
<div class="row companions-container-row">
</div>
`
    let i = 0
    let containerRow = document.querySelector('.companions-container-row')
    for (let companion of tripCompanions) {
        containerRow.innerHTML += /*html*/ `
            <div class="col-md-4 m-2 m-md-0">
                <div class="p-2 media-photo bg-image">
                    <div class="cardcontent">
                        <img
                            src=${img[i]}
                            class="img-fluid"
                            alt="companion3"
                        />
                        <div class="CompanionTitle">Name: ${companion.username}<br>E-mail: ${companion.email}</div>
                    </div>
                </div>
            </div>

        `
        i++
    }
}


// Delete Trip Plan
async function deleteTrip(tid) {
    const res = await fetch(`/trip/${tid}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
        },
    })
    if (res.status === 200) {
        setTimeout(() => {
            window.location.replace("/")
        }, 1000)

    }
}


// Show All planned trip details
async function showMyTripDetail() {
    const myTripContainer = document.querySelector('.my-trip')
    let html = ''
    const tripInfoRes = await fetch('/tripInfo')
    const tripsInfo = await tripInfoRes.json()
    for (let trip of tripsInfo) {
        console.log('tripCity: ', trip.city)
        html += /*html*/ `<div class="col-md-4">
            <div class="p-2 media-photo bg-image-card card trip-card">
                <div class="cardcontent" style="position: relative">
                    <img src="images/companion2.jpeg" class="img-fluid" alt="companion2" data-trip=${trip.id} data-tripcity='${trip.city}' data-triplength=${trip.day_length} />
                    <div class="trip-title" id="global">${trip.day_length} Days - ${trip.city}</div>
                    <div class="trip-period">
                    ${trip.start_date} - ${trip.end_date}
                    <div class="trip-date"><i class="fas fa-times" data-trip=${trip.id}  ></i></div>
                    </div>
                </div>
            </div>
        </div>`
    }
    myTripContainer.innerHTML = html

    const allTripPlan = document.querySelectorAll('.cardcontent img')
    for (let tripPlan of allTripPlan) {
        tripPlan.addEventListener('click', async (e) => {
            const chosenTripID = tripPlan.dataset.trip
            const chosenTripCity = tripPlan.dataset.tripcity
            const chosenTripLength = tripPlan.dataset.triplength
            const formObject = {
                chosenTripID,
                chosenTripCity,
                chosenTripLength,
            }
            const updateSessionRes = await fetch('/tripSession', {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(formObject),
            })
            if (updateSessionRes.status === 200) {
                window.location = '/main.html'
            }
        })
    }
}

// Search Bar Form Submission
search_form.addEventListener('submit', async (e) => {
    e.preventDefault()
    let formObject = {}
    formObject['city'] = search_form['city'].value[0].toUpperCase() + search_form['city'].value.slice(1)
    formObject['start_date'] = search_form['start_date'].value
    formObject['end_date'] = search_form['end_date'].value
    const res = await fetch('/itineraryDetails', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(formObject),
    })
    if (res.status === 200) {
        window.location = '/main.html'
    }
})