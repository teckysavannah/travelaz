export function checkSignInModal() {
  document.querySelector('.signInModal').innerHTML = /*html*/ `
<!-- Modal -->
<div class="modal fade" id="signInModal" tabindex="-1" data-bs-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title" id="signInModal">OMGGGGGGGGGGGG!!!</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" disabled></button>
      </div>
      <div class="modal-body">
      Please login first.
      </div>
      <div class="modal-footer">
      <a href="/" >
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Byeeeeeeee</button></a>
        <a href="/login.html" >
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Login</button></a>
      </div>
    </div>
  </div>
</div>
`
}
