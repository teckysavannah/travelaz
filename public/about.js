import { checkAuthState } from './checkAuthSate.js'
import { header } from './navbar.js'
window.onload = async () => {
    header()
    await checkAuthState()
}

