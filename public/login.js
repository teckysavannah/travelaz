const registerBtn = document.querySelector('.register-btn')
const login = document.querySelector('.login')
const register = document.querySelector('.register')
registerBtn.addEventListener('click', () => {
    login.style.display = 'none'
    register.style.display = 'block'
})

// Sign in anchor
const signInAnchor = document.querySelector('.register a')
signInAnchor.addEventListener('click', () => {
    window.location = '/login.html'
})

// Register Form Submission
const registerForm = document.getElementById('register-form')
registerForm.addEventListener('submit', async function (e) {
    e.preventDefault()
    const register_username = registerForm['register_username'].value
    const register_email = registerForm['register_email'].value
    const register_password = registerForm['register_password'].value
    const formObject = { register_username, register_email, register_password }
    const res = await fetch('/register', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(formObject),
    })
    registerForm.reset()
    if (res.status === 200) {
        window.location = '/login.html'
        const resp = await res.json()
        alert(resp.message)
    } else {
        const errMessage = (await res.json()).message
        alert(errMessage)
    }
})

// Login Form Submission
const loginForm = document.getElementById('login-form')
loginForm.addEventListener('submit', async function (e) {
    e.preventDefault()
    const email = loginForm['email'].value
    const password = loginForm['password'].value
    const formObject = { email, password }
    console.log(formObject)
    const res = await fetch('/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(formObject),
    })
    loginForm.reset()
    if (res.status === 200) {
        window.location = '/'
    } else {
        const errMessage = (await res.json()).message
        alert(errMessage)
    }
})
