document.querySelector('.search-container').innerHTML =
  /*html*/
  `
  <div class="container-lg">
    <form class="" id="search-form">
        <h4 >Book your trip</h3>
        <div class="content">
          <div class="search-bar">
            <div class="destination">
              <div class="icon"><i class="bi bi-house"></i></div>
              <div>
                <h5>Enter Destination</h5>
                <input type="text" placeholder="Your Destination" name="city" required autocomplete="off" />
              </div>
            </div>
            <div class="check-date">
              <div class="check-in position-relative">
                <div class="icon"><i class="bi bi-calendar"></i></div>
                <div class="">
                  <h5 id="demo">Check-in</h5>
                  <input type="text" id="check-in-datepicker" name="start_date" required autocomplete="off" >
                  <div
                    class="calendar display-none"
                    id="check_in_calendar"
                  ></div>
              </div>
              </div>
              <div class="check-out position-relative">
                <div class="icon"></div>
                <div class="">
                  <h5>Check-out</h5>
                  <input type="text" id="check-out-datepicker" name="end_date" required autocomplete="off" >
                  <div
                    class="calendar display-none"
                    id="check_out_calendar"
                  ></div>
                </div>
              </div>
            </div>
          </div>
          <div class="search-btn">
            <button type="submit" class="tripBtn">See your trip</button>
          </div>
        </div>
      </form>
    </div>
`
$(function () {
  $('#check-in-datepicker').datepicker()
  $('#check-out-datepicker').datepicker()
})
