import { checkAuthState } from './checkAuthSate.js'
import { header } from './navbar.js'

window.onload = async () => {
    header()
    await checkAuthState()
}

document.querySelector('#feedback')
.addEventListener('submit',async function(event){
    event.preventDefault();
    // Serialize the Form afterwards
    const form = event.target;
    const formObject = {};
    formObject['name'] = form.name.value;
    formObject['email'] = form.email.value;
    formObject['feedbackText'] = form.feedbackText.value;
    const res = await fetch('/contact',{
        method:"POST",
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(formObject)
    });
    if (res.status === 200) {
        Swal.fire({
            icon: 'success',
            title: 'Thank you for your feedback',
            showConfirmButton: false,
            timer: 1500
          })
          setTimeout(() => {
              window.location = '/index.html'
          }, 1000) 
}
})
